#include <systemc.h>
#include "obj_dir/Vpgcd.h"

// La fonction PGCD (sans contrainte temporelle)
sc_uint<8> pgcd(sc_uint<8> a, sc_uint<8> b) { 

    sc_uint<8> temp ;  
    sc_uint<8> max = a ;
    sc_uint<8> min = b ;
 

    if (max < min) {
        temp = min ;
        min = max ;
        max = temp ;
    }

    while(min != 0){
        max = max - min ; 

        if (max < min) {
        temp = min ;
        min = max ;
        max = temp ;
        }
    }

    return max ; 
}

//////////////////////////////////////////////////////////////////////////
///////////////////////// Représentation temporelle //////////////////////
//////////////////////////////////////////////////////////////////////////
SC_MODULE(PGCD) { 
    
    //Entrées
    sc_in<bool> clk ;
    sc_in<bool> valid ;
    sc_in<sc_uint<8>> A ; 
    sc_in<sc_uint<8>> B ;

    //Sorties
    sc_out<bool> ready ;
    sc_out<sc_uint<8>> result ; 

    sc_uint<8> pgcd_in(sc_uint<8> a, sc_uint<8> b) { 

        sc_uint<8> temp ;  
        sc_uint<8> max = a ;
        sc_uint<8> min = b ;
    

        if (max < min) {
            temp = min ;
            min = max ;
            max = temp ;
        }

        while(min != 0){
            max = max - min ; 

            if (max < min) {
            temp = min ;
            min = max ;
            max = temp ;
            }

            wait(1) ; 
        }

        return max ; 
    }   

    void compute() {

        ready = 0 ; 
        result = 0 ; 

        for(;;) {
            
            if(valid) {
                result = pgcd_in(A,B) ;
                ready = 1 ;
                wait(1) ; 
                ready = 0 ;
                result = 0 ;
            }

            wait() ; 
            
        }
    }

    SC_CTOR(PGCD) { 
        SC_CTHREAD(compute, clk.pos()) ;
    }

} ;
//////////////////////////////////////////////////////////////////////////
//////////////////////////// Représentation RLT //////////////////////////
//////////////////////////////////////////////////////////////////////////

SC_MODULE(PGCD_RTL) { 

    //Ports en entrée
    sc_in<bool> clk ;
    sc_in<sc_uint<8>> A ;
    sc_in<sc_uint<8>> B ;
    sc_in<bool> valid ;  

    //Ports en sortie
    sc_out<sc_uint<8>> result ;
    sc_out<bool> ready ; 

    //Signaux internes
    sc_signal<sc_uint<8>> max ; 
    sc_signal<sc_uint<8>> min ;


    void compute() {

        //Controle de ready
        if (min.read() == sc_uint<8>(0) && !valid){
            ready = true ;
        }     
        else {
            ready = false ;
        }   

        //Controle des bascules internes
        if (valid) {
            max = B ;
            min = A ;
            if (A.read() > B.read()){
                max = A;
                min = B ;
            }
        }

        else {
            sc_uint<8> tmp = max.read() - min.read() ;
            
            max = tmp ;
            if (tmp < min) {
                max = min ; 
                min = tmp ;
            }
        }

        //Sortie
        result = max ;
    }

    SC_CTOR(PGCD_RTL) {
        SC_METHOD(compute) ;
        sensitive << clk.pos() ;
    }
};

//////////////////////////////////////////////////////////////////////////
/////////////////////// Comparaison enntre méthodes //////////////////////
//////////////////////////////////////////////////////////////////////////
void compare(sc_uint<8> A, sc_uint<8> B, sc_uint<8> result, bool thread) {
    cout << "# Comparaison des valeurs de PGCD(" << A << ", " <<  B << "): " << endl ; 
    cout << "     Fonction:   " << pgcd(A,B) << endl ; 
    if (thread) cout << "     SC_CTHREAD: " << result << endl ;  
    else cout << "     SC_METHOD: " << result << endl ;

    if (result != pgcd(A,B)) {
        cout << "############ Error here #########" << endl ;
    }

}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////// Simulation //////////////////////////////
//////////////////////////////////////////////////////////////////////////
int sc_main(int argc, char * argv[]) {

    cout << "############### Test de la fonction #################################" << endl ;


    cout << "PGCD(25,10)= " << pgcd(25,10) << endl ;
    cout << "PGCD(12,3)= " << pgcd(12,3) << endl ;
    cout << "PGCD(19,12)= " << pgcd(19,12) << endl ; 

    cout << "############### Test du module utilisant des SC_THREA ou SC_METHOD #############" << endl ;
    
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////// SC_THREAD //////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    //////////////////////// Déclarations //////////////////////
    sc_signal<sc_uint<8>> A,B,result ;
    sc_clock clk("clk",10,SC_NS) ; 
    sc_signal<bool> valid, ready ; 

    //PGCD PGCD_i("PGCD_i") ; 

    sc_time T(10,SC_NS) ; 

    /////////////////////// Connection des ports ///////////////////////
    /* PGCD_i.clk(clk) ;
    PGCD_i.A(A) ;
    PGCD_i.B(B) ;
    PGCD_i.result(result) ;
    PGCD_i.valid(valid) ;
    PGCD_i.ready(ready) ;

    */

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////// SC_METHOD //////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    

    PGCD_RTL PGCD_RTL_i("PGCD_RTL_i") ;

    PGCD_RTL_i.clk(clk) ;
    PGCD_RTL_i.A(A) ;
    PGCD_RTL_i.B(B) ;
    PGCD_RTL_i.result(result) ;
    PGCD_RTL_i.ready(ready);
    PGCD_RTL_i.valid(valid) ; 
    
    ///////////////////// Trace et simulation //////////////////////////////

    sc_trace_file * trace ; 
    trace = sc_create_vcd_trace_file("pgcd") ;
    trace->set_time_unit(1,SC_NS) ; 

    //Les signaux à tracer
    sc_trace(trace, clk, "clk") ;
    sc_trace(trace, A, "A") ;
    sc_trace(trace, B, "B") ;
    sc_trace(trace, result, "result") ;
    sc_trace(trace, ready, "ready") ;
    sc_trace(trace, valid, "valid") ; 

    //On simule notre PGCD exhaustif
    for(int i = 1; i<100; i++) {
        valid = 0 ; 
        A = i ; 
        for(int j = 1; j < 100; j++) {
            B = j ; 
            valid = 1 ; 
            sc_start(T); 
            valid=0 ;
            while(!(ready)) {
                sc_start(T); 
                //cout<<"Wait again: " << sc_time_stamp() << endl ;
            }
            compare(A,B,result, false) ; 
        }
    }

    sc_stop() ; 

    return 0 ;
}

/* Remarque : Je ne suis pas arrivé à traiter et simuler les deux en simultané
Il faut peut être maintenir ready pour faire cela */ 