#include <systemc.h>

SC_MODULE(Test) { 

    sc_port<sc_signal_in_if<sc_uint<8>>,1> sig ; 
    sc_port<sc_signal_in_if<sc_uint<8>>,1> buf ;

    void foo() {
        cout << "Buffer notified ! " << endl ; 
    }

    void bar() { 
        cout << "Signal notified !" << endl ; 
    }

    SC_CTOR(Test) {
        SC_METHOD(foo) ;
        sensitive << buf ;
        SC_METHOD(bar) ; 
        sensitive << sig ;
    }
} ; 

int sc_main(int argc, char * arv[]) { 

    Test test("test") ; 

    sc_signal<sc_uint<8>> sig ; 
    sc_buffer<sc_uint<8>> buf ; 

    test.sig(sig) ; 
    test.buf(buf) ; 

    cout << "Premier test" << endl ; 

    sig = 0 ; 
    buf = 0 ; 

    sc_start(5,SC_NS) ; 

    cout << "Deuxième test" << endl ;

    sig = 1 ; 
    buf = 1 ; 

    sc_start(5,SC_NS) ;

    cout << "Troisième test" << endl ;

    sig = 1 ; 
    buf = 1 ; 

    sc_start(5,SC_NS) ; 

    return 0 ; 
}
