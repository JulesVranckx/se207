#include <systemc.h> 

// Le type pixel
struct pixel_t {
    sc_uint<5> R;
    sc_uint<6> G;
    sc_uint<5> B;
    // Mon contructeur
    pixel_t( sc_uint<5> _R = 0,sc_uint<6> _G = 0, sc_uint<5> _B=0): R(_R), G(_G), B(_B) {}

    bool operator == (const pixel_t &other) const {
        return (R == other.R) && (G == other.G) && (B == other.B) ;
    }

    friend ostream& operator << ( ostream& o, const pixel_t& P ) {
        o << "(" << P.R << "," << P.G << "," << P.B << ")" ;
        return o;
    }

    //Surcharge de l'addition (pix + entier)
    pixel_t operator+(const unsigned int n) const {
      sc_uint<5> r = R + n ;
      sc_uint<6> g = G + n ;
      sc_uint<5> b = B + n ;

      return {r,g,b} ;
    }

    //Surcharge de l'addition (pix + pix) 
    pixel_t operator+(const pixel_t & other) const { 
        sc_uint<5> r = R + other.R ; 
        sc_uint<6> g = G + other.G ;
        sc_uint<5> b = B + other.B ; 

        return {r,g,b} ;
    }
};


// surcharge de l'addition (entier + pixel)
pixel_t operator+(const unsigned int n, const pixel_t p) {
  return p+1;
}

// surcharge de la fonction sc_trace pour le type utilisateur
void sc_trace( sc_trace_file* _f, const pixel_t& _foo, const std::string& _s ) {
    sc_trace( _f, _foo.R, _s + "_R" );
    sc_trace( _f, _foo.G, _s + "_G" );
    sc_trace( _f, _foo.B, _s + "_B") ;
}


SC_MODULE(producer_thread) {
    sc_in<bool> clk ; 
    sc_fifo_out<pixel_t> out; 

    void write() {
        
        pixel_t pix ;

        for(;;) { 
             
            for (int i = 0; i < 100 ; i++) { 
                pix = {rand(), rand(), rand()} ; 
                cout << "Write :" << pix << endl ; 
                out.write(pix) ; 
                wait(1) ; 
            }

            wait(rand() % 32 ) ; 
        }
    }

    SC_CTOR(producer_thread) { 
        SC_CTHREAD(write, clk.pos()) ; 
    }

} ; 

SC_MODULE(producer_method) {
    sc_in<bool> clk ;
    sc_fifo_out<pixel_t> out ; 

    //Signaux de controle
    int nbr_data = 0 ; 
    bool wait_1 = false;
    bool wait_rand = false ; 
    int rand_cmpt = rand() ; 
    pixel_t pix ;
    bool ok ;
    
    void write(){

        if (nbr_data == 100) { 
            nbr_data = 0 ; 
            wait_1 = false ;
            wait_rand = true ; 
        }

        else if (wait_1) {
            wait_1 = 0 ; 
        }

        else if (wait_rand) { 
            if (rand_cmpt == 0 ) {
                wait_rand  =false ; 
                rand_cmpt = rand() ; 
            }
            else {
                rand_cmpt-- ; 
            }
        }
        else {
            pix = {rand(), rand(), rand()} ;
            cout << "Write " << pix << endl ;
            ok = out.nb_write(pix) ; 
            if (ok) { 
                nbr_data++ ;
                wait_1 = true;
            }
        }
    }

    SC_CTOR(producer_method) {
        SC_METHOD(write) ;
        sensitive << clk.pos() ;
        dont_initialize() ;
    }
} ;

SC_MODULE(consumer) { 
    sc_in<bool> clk ; 
    sc_fifo_in<pixel_t> in ; 

    void read() {

        pixel_t pix ;

        for(;;) {

            pix = in.read() ;   
            cout << "Read data: " << pix << endl ; 
            int sum = pix.R + pix.G + pix.B ; 

            if (sum%2 == 0) {
                cout << "Somme paire" << endl ; 
                wait(2) ; 
            }
            else {
                cout << "Somme impaire" << endl ;
                wait(3) ; 
            }
        }
    }

    SC_CTOR(consumer) {
        SC_CTHREAD(read, clk.pos()) ; 
    }
} ;

int sc_main(int argc, char * argv[]) { 

    if (argc != 2) {
        cout << "Wrong usage. Please use ./fifo FIFO_SIZE " << endl ; 
        return 1 ;
    }

    int FIFO_SIZE = atoi(argv[1]) ; 
    sc_fifo<pixel_t> fifo(FIFO_SIZE)  ; 

    sc_clock clk("clk", 10, SC_NS) ; 

    consumer consumer("Consumer") ; 
    producer_method produc1("Producer Method ") ; 

    consumer.clk(clk) ; 
    consumer.in(fifo) ;

    produc1.clk(clk) ; 
    produc1.out(fifo) ; 

    sc_start(2000, SC_NS) ; 

    cout << "FIFO: " << endl << fifo << endl ;
    
    return 0 ;
}