#include <systemc.h>

SC_MODULE(foo) {
   sc_in <bool> i;
   sc_out<bool> o;
   SC_CTOR(foo):i("i"),o("o") { }
};

SC_MODULE(bar) {
   sc_in <bool> i;
   sc_out<bool> o;
   SC_CTOR(bar):i("i"),o("o") { }
};

SC_MODULE(foobar)
{
   // entrée/sortie
   sc_in <bool> i;
   sc_out<bool> o;
   // interne
   sc_signal<bool> s;
   // sous modules
   foo foo_i;
   bar bar_i;

   SC_CTOR(foobar)
      :i("i"),o("o"),s("s"),
      foo_i("foo"), bar_i("bar")
   {
      // connexions aux I/O
      foo_i.i(i);
      bar_i.o(o);
      // connexion interne
      foo_i.o(s);
      bar_i.i(s);
   }
};

int sc_main(int argc, char * argv[])
{
   sc_signal<bool> i("i");
   sc_signal<bool> o("o");

   foobar uut("foobar");

   //uut.i(i);
   uut.o(o);

   sc_clock ck1("ck1", 10, SC_NS) ;

    uut.i(ck1) ;

    sc_trace_file * trace ;
    trace = sc_create_vcd_trace_file("instance") ;
    trace->set_time_unit(1,SC_NS);
    sc_trace(trace, uut.i, "uut.i") ;
    sc_trace(trace, uut.o, "uut.o") ;
    sc_trace(trace, uut.s, "uut.s") ;
    sc_trace(trace, uut.foo_i.i, "uut.foo.i") ;
    sc_trace(trace, uut.foo_i.o, "uut.foo.o") ;
    sc_trace(trace, uut.bar_i.i, "uut.bar.i") ;
    sc_trace(trace, uut.bar_i.o, "uut.bar.o") ;


    sc_start(500,SC_NS);
    sc_stop() ;


   return 0;
}