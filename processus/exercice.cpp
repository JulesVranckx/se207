#include <systemc.h>

// Le type pixel
struct pixel_t {
    sc_uint<5> R;
    sc_uint<6> G;
    sc_uint<5> B;
    // Mon contructeur
    pixel_t( sc_uint<5> _R = 0,sc_uint<6> _G = 0, sc_uint<5> _B=0): R(_R), G(_G), B(_B) {}

    bool operator == (const pixel_t &other) const {
        return (R == other.R) && (G == other.G) && (B == other.B) ;
    }

    friend ostream& operator << ( ostream& o, const pixel_t& P ) {
        o << "(" << P.R << "," << P.G << "," << P.B << ")" ;
        return o;
    }

    //Surcharge de l'addition (pix + entier)
    pixel_t operator+(const unsigned int n) const {
      sc_uint<5> r = R + n ;
      sc_uint<6> g = G + n ;
      sc_uint<5> b = B + n ;

      return {r,g,b} ;
    }

    //Surcharge de l'addition (pix + pix) 
    pixel_t operator+(const pixel_t & other) const { 
        sc_uint<5> r = R + other.R ; 
        sc_uint<6> g = G + other.G ;
        sc_uint<5> b = B + other.B ; 

        return {r,g,b} ; 
    }

};

// surcharge de l'addition (entier + pixel)
pixel_t operator+(const unsigned int n, const pixel_t p) {
  return p+1;
}

//Somme saturante de sc_uint<5> 

sc_uint<5> satur_sum(const sc_uint<5> A, sc_uint<5> B) {
    uint a = A ;
    uint b = B ;
    sc_uint<5> sum ; 
    cout << "a :" << a << " A: " << A << endl ; 
    cout << "b :" << b << " B: " << B << endl ; 


    if ((a+b) >= 31) {
        sum = 31 ; 
    }
    else {
        sum = A + B ; 
    }

    cout << "Sum: " << sum << endl ;

    return sum ;
}

//Somme saturante de sc_uint<6> (avec duplication de code ...)

sc_uint<6> satur_sum(const sc_uint<6> A, sc_uint<6> B) {
    uint a = A ;
    uint b = B ;
    sc_uint<6> sum ; 

    if ((a+b) >= 63) {
        sum = 63 ; 
    }
    else {
        sum = A + B ; 
    }

    return sum ;
}

//Somme saturante de pixels

pixel_t satur_sum(const pixel_t P1, const pixel_t P2) {
    sc_uint<5> R = satur_sum(P1.R, P2.R) ;
    sc_uint<6> G = satur_sum(P1.G, P2.G) ;
    sc_uint<5> B = satur_sum(P1.B, P2.B) ;

    return {R,G,B} ;
}

// surcharge de la fonction sc_trace pour le type utilisateur
void sc_trace( sc_trace_file* _f, const pixel_t& _foo, const std::string& _s ) {
    sc_trace( _f, _foo.R, _s + "_R" );
    sc_trace( _f, _foo.G, _s + "_G" );
    sc_trace( _f, _foo.B, _s + "_B") ;
}

//Premier Module, Somme avec SC_METHOD
SC_MODULE(sum_1) { 

    sc_in<pixel_t> pixel1 {"pixel1"};
    sc_in<pixel_t> pixel2 {"pixel2"};
    sc_out<pixel_t> sum {"sum"}; 

    SC_CTOR(sum_1) {
        SC_METHOD(compute) ; 
        sensitive << pixel1 << pixel2 ;
        dont_initialize() ;
    }

    void compute() {
        sum = satur_sum(pixel1.read(), pixel2.read()) ;
    }

} ;

//Second Module, Somme avec SC_THREAD
SC_MODULE(sum_2) {

    sc_in<pixel_t> pixel1 {"pixel1"};;
    sc_in<pixel_t> pixel2 {"pixel2"};;
    sc_out<pixel_t> sum {"sum"}; 

    SC_CTOR(sum_2) { 
        SC_THREAD(compute) ; 
        sensitive << pixel1 << pixel2 ; 
    }

    void compute() {

        for(;;) {

            sum = satur_sum(pixel1.read(), pixel2.read()) ; 
            wait() ; 
        }
    }
} ;

//Troisieme Module, Generateur de couleur (SC_METHOD)
SC_MODULE(all_colors_1) {

    sc_in<bool> clk; 
    sc_in<bool> r ;
    sc_out<pixel_t> pixel ;

    SC_CTOR(all_colors_1) {

        SC_METHOD(next_color) ;
        sensitive << clk.pos() ; 
        async_reset_signal_is(r,true) ;
        dont_initialize() ;
    }

    void next_color() {

        if (r) {

            pixel = pixel_t(0,0,0) ;

        }
        else { 

            sc_uint<5> next_R = pixel.read().R + 1 ; 
            sc_uint<6> next_G = pixel.read().G;
            sc_uint<5> next_B = pixel.read().B ;

            if(next_R == 0 ) { 

                next_G += 1 ; 

                if(next_G == 0) {

                    next_B += 1 ;
                }

            }
            pixel = pixel_t(next_R,next_G,next_B) ;
        }
    }
} ; 

//Quatrieme module, generateur de couleurs (SC_THREAD)
SC_MODULE(all_colors_2) {

    sc_in<bool> clk; 
    sc_in<bool> r ;
    sc_out<pixel_t> pixel ;

    SC_CTOR(all_colors_2) {

        SC_THREAD(next_color) ;
        sensitive << clk.pos() ;
        async_reset_signal_is(r,true) ;

    }

    void next_color() {

        pixel = pixel_t(0,0,0) ;
         

        for(;;) { 

            wait() ; 

            sc_uint<5> next_R = pixel.read().R + 1 ; 
            sc_uint<6> next_G = pixel.read().G;
            sc_uint<5> next_B = pixel.read().B ;

            if(next_R == 0 ) { 

                next_G += 1 ; 

                if(next_G == 0) {

                    next_B += 1 ;
                }

            }

            pixel = pixel_t(next_R,next_G,next_B) ;

        }
    }
} ;

SC_MODULE(all_colors_3) {

    sc_in<bool> clk; 
    sc_in<bool> r ;
    sc_out<pixel_t> pixel ;

    SC_CTOR(all_colors_3) {

        SC_CTHREAD(next_color, clk.pos()) ;
        async_reset_signal_is(r,true) ;

    }

    void next_color() {

        pixel = pixel_t(0,0,0) ;
        
        for(;;) { 

            wait() ;

            sc_uint<5> next_R = pixel.read().R + 1 ; 
            sc_uint<6> next_G = pixel.read().G;
            sc_uint<5> next_B = pixel.read().B ;

            if(next_R == 0 ) { 

                next_G += 1 ; 

                if(next_G == 0) {

                    next_B += 1 ;
                }

            }

            pixel = pixel_t(next_R,next_G,next_B) ;

        }
    }
};

int sc_main(int argc, char * argv[]) {

    sc_clock clk("clk",10, SC_NS) ;
    sc_signal<bool> r("reset", true) ; 
    sc_signal<pixel_t> op1, op2, somme1, somme2 ;
    sc_signal<pixel_t> color1, color2, color3 ; 

    //Les modules pour la somme
    sum_1 sum_1_i("sum_1_i") ;
    sum_2 sum_2_i("sum_2_i") ;

    sum_1_i.pixel1(op1) ;
    sum_1_i.pixel2(op2) ;
    sum_1_i.sum(somme1) ;

    sum_2_i.pixel1(op1) ; 
    sum_2_i.pixel2(op2) ;
    sum_2_i.sum(somme2) ;  

    //Les modules pour generer les couleurs
    all_colors_1 all_colors_1_i("all_colors_1_i") ; 
    all_colors_2 all_colors_2_i("all_colors_2_i") ;
    all_colors_3 all_colors_3_i("all_colors_3_i") ;

    all_colors_1_i.clk(clk) ;
    all_colors_1_i.r(r) ;
    all_colors_1_i.pixel(color1) ; 

    all_colors_2_i.clk(clk) ;
    all_colors_2_i.r(r) ;
    all_colors_2_i.pixel(color2) ;

    all_colors_3_i.clk(clk) ;
    all_colors_3_i.r(r) ;
    all_colors_3_i.pixel(color3) ;

    // Debut de la simulation 

    sc_trace_file * trace ; 
    trace = sc_create_vcd_trace_file("all_modules") ; 
    trace->set_time_unit(1,SC_NS) ; 

    sc_trace(trace, somme1, "somme_Method") ; 
    sc_trace(trace, somme2, "Somme_Thread") ; 

    sc_trace(trace, clk, "clk") ; 
    sc_trace(trace, r, "reset") ; 
    sc_trace(trace, color1, "Couleur_Method") ; 
    sc_trace(trace, color2, "Couleur_Thread") ; 
    sc_trace(trace, color3, "Couleur_CThread") ; 

    op1 = pixel_t(1,2,3) ;
    op2 = pixel_t(1,2,4) ; 

    sc_start(10,SC_NS) ;

    op1 = pixel_t(16,32,16) ; 
    op2 = pixel_t(16,32,15) ; 
    r = false ;

    sc_start(100000,SC_NS) ;

    r = true ; 

    sc_start(10000, SC_NS) ;  

    r = false ;

    sc_start(100000,SC_NS) ;

    sc_stop() ; 

    return 0 ;
}