# Projet SystemC: filtre et flux vidéo

## Module de réception vidéo

Pour ce module, je n'ai pas rencontré de difficultés particulières. 

J'ai par contre réalisé deux versions : 

- Une première se basant sur le code donné de `video_in.cpp`, utilisant un SC_THREAD et une allocation dynamique qui n'était pas appropriée
- Suite à des conseils, j'ai donc ré-écris le code en utilisant une SC_METHOD (car le traitement y était plus simple et surtout plus compréhensible je trouve) et une allocation statique (qui suffit ici).

Le module dans le fichier `video_out.cpp` semble donc être correct. On ne distingue pas d'erreur de pixels en comparant les images avec `eog` par exemple.

## Module Filtre moyenneur

Pour ce filtre, j'ai rencontré un peu plus de difficultés. J'ai utilisé un code proche du module précédent pour la réception des pixels. Cependant, au lieu d'écrire un `.png`, il écrit dans un buffer. Buffer qui est d'ailleurs de taille (3*WIDTH), car on applique un filtre utilisant les 8 pixels adjacents (il faut donc une ligne en dessus et une en dessous).  J'utilisais au début une variable `buff_full`, qui me permettait de savoir si je pouvais commencer l'écriture de l'image ou non. 

Pour la partie écriture d'image, je n'ai donc pas utilisé la variable ci-dessus au final. Je commence à écrire la nouvelle image lorsque j'ai reçu une ligne et deux pixels exactement. En effet, à partir de ce moment là, on peut appliquer le filtre au premier pixel. 

Pour écrire, je fais varier deux indices de boucles qui se chargeront de représenter la position du pixel écrit (variable `pix` dans le code). A partir des ces indices, et pour chaque pixel, je place dans une variable `somme` la somme des pixels adjacents. J'utilise là encore des boucles for. J'ai pendant longtemps oublié de remettre la variable  `somme` à 0 à chaque pixel, ce qui a posé des problèmes évidemment. 

A la fin de la génération d'une ligne, je met `href_out ` à 0 suivant le protocole. Je refais cela le bon nombre de lignes aussi à la fin de l'image. 

Dans le fichier `system.cpp`, j'ai ensuite ajouter mon module et connecter les ports. 

Le filtre moyenneur semble fonctionner dans les tests (chronogrammes et résultats visuels). 

## Adaptation du filtre

Avant de passer au zoom, je choisis de rendre mon module de filtre adaptable à d'autre filtres. Il faut donc pour cela changer la fonction d'écriture, pour qu'elle utilise le filtre spécifié. On spécifie le filtre voulu en ligne de commande lors du lancement de la simulation, parmi une liste de filtres présents dans le fichier `filtres.h`. 

Un objet du module `FILTRE` représente donc cette fonction, et elle est appelée dans `send_pic()`. 

Le filtre est choisi au moment de l'appel au constructeur. 

Pour l'instant, on donne des coefficients parlesquels multiplier les pixels obtenus. 

## Zoom x2 centré

Pour le zoom, je suis parti sur l'idée de stocker dans un buffer un quart des pixels (centraux). En dupliquant chaque pixel pour en faire 4, on fait un zoom x2 sur la largeur, et un zoom fois deux sur la hauteur.
Je ne suis pas exactement sur que c'est ce qui est demandé, mais j'implémente une première version de la sorte.
Je pense que cela ne demandera pas de changements majeurs pour une autre "taille" de zoom. 

