// Implémentation des méthodes

#include "filtre.h"

void FILTRE::fill_buff() {

    if (!reset_n) {
        cout << "module: FILTRE... reset !" << endl ;
        old_vref = false ; 
        crrt_pix = 0 ; 
        EOL = 0 ;
        return ; 
    }

    bool new_image = vref_in & !old_vref ; 

    buff_ready = (crrt_pix == WIDTH + 2) ;

    if (new_image) { 
        crrt_pix = 0 ;
    }

    if (href_in) {
        buff[crrt_pix] = pixel_in ; 
        crrt_pix = (crrt_pix + 1) %MAX_BUFF ;
    }

    if (!href_in) {
        compt += 1 ;
    }

    if (!old_href & href_in) {
        EOL = compt ;
        compt = 0 ;
    }

    old_vref = vref_in ;
    old_href = href_in ; 

}

void FILTRE::send_pic() {
        
    href_out = false ;
    vref_out = false ; 
    pixel_out = 0 ; 

    int pix = 0 ;

    for (;;) {
        
        //Wait for to have enough pixels to begin 
        // (For the first pixel, wwe need a line and two 
        // pixels to compute)
        while (!buff_ready) { wait() ;}
        pix = 0 ;

        for (int x = 0 ; x < HEIGHT; x++) { 

            for (int y = 0; y < WIDTH ; y++) { 
                vref_out = (x < 3) ; 
                pix = (x * WIDTH + y ) % MAX_BUFF; 

                //Ecriture d'un pixel actif 
                href_out = true ; 
                pixel_out = filter(pix) ;
                wait() ;
                
            }

            //Fin de ligne
            href_out = false ;
            pixel_out = 0 ;
            for (int y = 0; y < EOL; y++) {
                wait() ;
            }
        }
    }
}