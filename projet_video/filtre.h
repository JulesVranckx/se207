// Filtre moyenneur (certainement plus tard adapté en filtre agissant sur son
// voisinnage 3*3)

#ifndef FILTRE_H
#define FILTRE_H

#include <systemc.h>


using namespace std ;

SC_MODULE(FILTRE) { 

    //Ports 
    sc_in<bool> clk ; 
    sc_in<bool> reset_n ;

    sc_in<unsigned char> pixel_in ; 
    sc_in<bool> href_in ; 
    sc_in<bool> vref_in ; 

    sc_out<unsigned char> pixel_out ; 
    sc_out<bool> vref_out ;
    sc_out<bool> href_out ;

    // Constructeur

    SC_HAS_PROCESS(FILTRE) ;

    FILTRE(sc_module_name n, std::string func):
        sc_module(n)
    { 

        cout << "Instanciation du filtre moyenneur" << endl ;

        SC_METHOD(fill_buff) ;
        sensitive << clk.pos() ; 
        async_reset_signal_is(reset_n, false) ;
        dont_initialize() ;

        SC_CTHREAD(send_pic, clk.pos()) ;
        async_reset_signal_is(reset_n, false) ;

        if (func.compare("moy") == 0) {
            //Coefficients du filtre moyenneur
            for (int i = 0; i < 9 ; i++) {
                coeff[i] = 1/9. ;
            }
        }
        else if (func.compare("gauss")==0){
            //Coefficients du filtre moyenneur de Gauss
            coeff[0] = coeff[2] = coeff[6] = coeff[8] = 1/16. ;
            coeff[1] = coeff[3] = coeff[5] = coeff[7] = 1/8.;
            coeff[4] = 1/4. ;

        }
        else if (func.compare("laplacien") == 0){
            //Filtre Laplacien, detection de contours
            coeff[0] = coeff[2] = coeff[6] = coeff[8] = 0. ;
            coeff[1] = coeff[3] = coeff[5] = coeff[7] = 1.;
            coeff[4] = -4. ;
        }
        else {
            cerr << "Filtre non reconnu" << endl ;
            exit(-1) ;
        }
    }

    private : 

    //Constantes de taille pour les images
    static const int HEIGHT  = 576;
    static const int WIDTH   = 720;
    static const int fHEIGHT = HEIGHT+49;
    static const int fWIDTH  = WIDTH +154;
    static const int MAX_BUFF = 3 * WIDTH ; 

    // SC_METHOD and SC_CTHREAD

    //Receive the pixels and store them in a buffer before using
    void fill_buff() ;

    // Get the pixels in the buffer, applies the filter and send them
    void send_pic() ;

    // Filter function, use the coeff tab 
    unsigned char filter(int pix) {

        int adja[9] ;

        //Récupération des pixels dont on a besoin
        for (int i = -1; i < 2 ; i++) {
            for (int j = -1; j < 2; j++) {
                int index = (pix + j + i*WIDTH + MAX_BUFF) % MAX_BUFF ;
                adja [(i+1)*3 + (j+1)] = buff[index] ;
            }
        }

        //Traitement du pixel en sortie
        int pix_x = pix / WIDTH ;
        int pix_y = pix % WIDTH ;
        unsigned char pix_out =0 ;
        for (int i =0 ; i < 3; i++) {
            for (int j = 0 ; j < 3; j++) {
                pix_out += adja[i*3+j] * coeff[(3*(pix_x-i) + (pix_y - j) +9)%9] ;
            }
        }      
        return pix_out ;  
    }

    // Module objects

    unsigned char buff[3 * WIDTH] ; 
    bool old_vref ; 
    bool old_href ;
    int crrt_pix ;
    sc_signal<bool> buff_ready ;

    // Compteur du nombre inactif de pixels en fin de ligne (End Of Line)
    int compt ;
    sc_signal<int> EOL ;
    float coeff[9] ;
    
} ;
#endif // FILTRE_H