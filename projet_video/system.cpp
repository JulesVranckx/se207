/**********************************************************************
 * File : system.cpp
 * Date : 2008-2016
 * Author :  Alexis Polti/Tarik Graba
 *
 * This program is released under the GNU public license
 * Télécom ParisTECH
 *
 * Testbench pour video_in
 **********************************************************************/

#include <systemc.h>
#include <sstream>
#include "video_in.h"
#include "video_out.h"
#include "filtre.h"
#include "zoom.h"

/***************************************************
 *	MAIN
 **************************************************/

int sc_main (int argc, char *argv[])
{
    int	ncycles;

    if (argc == 3) {
        std::stringstream arg1(argv[1]);
        arg1 >> ncycles;
    } else {
        cout
           << endl
           << "Le nombre de cycles de simulation doit être passé en argument (-1 pour une simulation illimitée)"
           << endl
           << "Spécifiez le filtre choisi en deuxième argument dans la liste suivante :"
           << endl
           << "moy"
           << endl
           ;
        exit(1);
    }

    std::string func = std::string(argv[2]) ;

    /******************************************************
     *      Déclaration des signaux
     *****************************************************/

    /* La période de l'horloge du signal vidéo */
    sc_time pix_period(74, SC_NS);

    sc_clock                        signal_clk("Clock", pix_period);
    sc_signal<bool>                 signal_resetn;

    sc_signal<bool>                 signal_vref1, signal_href1;
    sc_signal<unsigned char>        signal_pixel1;

    sc_signal<bool>                 signal_vref2, signal_href2;
    sc_signal<unsigned char>        signal_pixel2;

    sc_signal<bool>                 signal_vref3, signal_href3;
    sc_signal<unsigned char>        signal_pixel3;

    /********************************************************
     *	Instanciation des modules
     *******************************************************/

    VIDEO_IN video_in("VIDEO_GEN");
    VIDEO_OUT video_out("OUTPUT") ;
    FILTRE filtre("FILTRE", func) ; 
    ZOOM zoom("ZOOM") ;
    

    /*********************************************************
     *	Connexion des composants
     ********************************************************/

    video_in.clk        (signal_clk);
    video_in.reset_n    (signal_resetn);
    video_in.href       (signal_href1);
    video_in.vref       (signal_vref1);
    video_in.pixel_out  (signal_pixel1);

    // Filtre
    filtre.clk        (signal_clk) ;
    filtre.reset_n    (signal_resetn) ;
    filtre.pixel_in   (signal_pixel1) ;
    filtre.href_in    (signal_href1) ;
    filtre.vref_in    (signal_vref1) ;
    filtre.pixel_out  (signal_pixel2) ;
    filtre.href_out   (signal_href2) ;
    filtre.vref_out   (signal_vref2) ;

    //zoom
    zoom.clk        (signal_clk) ;
    zoom.reset_n    (signal_resetn) ;
    zoom.pixel_in   (signal_pixel2) ;
    zoom.href_in    (signal_href2) ;
    zoom.vref_in    (signal_vref2) ;
    zoom.pixel_out  (signal_pixel3) ;
    zoom.href_out   (signal_href3) ;
    zoom.vref_out   (signal_vref3) ;

    video_out.clk       (signal_clk) ;
    video_out.reset_n   (signal_resetn);
    video_out.href      (signal_href3) ;
    video_out.vref      (signal_vref3) ;
    video_out.pixel_in  (signal_pixel3) ;

    /*********************************************************
     *	Traces
     ********************************************************/

    /* fichier de traces */
    sc_trace_file * my_trace_file;
    my_trace_file = sc_create_vcd_trace_file ("simulation_trace");
    my_trace_file->set_time_unit(1,SC_NS);

#define TRACE(x) sc_trace(my_trace_file, x, #x)

    /* chronogrammes signaux CLK et NRESET */
    TRACE( signal_clk );
    TRACE( signal_resetn );

    /* chronogrammes video */
    TRACE( signal_href1 );
    TRACE( signal_vref1 );
    TRACE( signal_pixel1 );
    TRACE( signal_href2) ;
    TRACE( signal_vref2) ;
    TRACE( signal_pixel2) ; 
    TRACE( signal_href3) ;
    TRACE( signal_vref3) ;
    TRACE( signal_pixel3) ;

#undef TRACE

    /*********************************************************
     *	Simulation
     ********************************************************/

    /* Initialisation de la simulation */
    sc_start(SC_ZERO_TIME);
    signal_resetn = true;
    sc_start(10*signal_clk.period());

    /* Génération d'un reset */
    signal_resetn = false;
    sc_start(10*signal_clk.period());
    signal_resetn = true;

    /* Lancement de la simulation */
    if(ncycles >= 0) {
       cout << "Simulation lancée pour " << ncycles << " cycle de " << signal_clk.period() << endl;
       sc_start(ncycles * signal_clk.period());
    } else {
       cout << "Simulation lancée en continu (CTRL-C pour l'arrêter)" << endl;
       sc_start();
    }

    cout << "Fin de la simulation @ " << sc_time_stamp() << endl;

    /* Close trace file */
    sc_close_vcd_trace_file (my_trace_file);

    return EXIT_SUCCESS;
}

