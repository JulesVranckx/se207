// Instanciation des méthodes du module 

#include "video_out.h"
#include <sstream>
#include <cstdio>
#include <iomanip>

// Tread de génération des images 

void VIDEO_OUT::gen_image() { 

    if (!reset_n) {
        current_image_number = 0 ; 
        old_vref = false ;
        cout << "Module: " << name() << "... reset!" << endl ;
        return ; 
    }

    bool new_image = vref & !old_vref ; 

    if (new_image) {
        crrt_pix = 0 ; 
    }

    if (href) { 
        image.pixel[crrt_pix] = pixel_in ; 
        crrt_pix++ ;
    }

    if (crrt_pix >= HEIGHT*WIDTH) {
        write_image() ;
        current_image_number++ ;
        crrt_pix = 0 ; 
    }

    old_vref = vref ; 

}


void VIDEO_OUT::write_image() {

    std::ostringstream name_buff ;

    //Calcul du nom de la sortie
    name_buff << name() << std::setfill('0') << std::setw(2) << current_image_number << ".png";

    image_write(&image, name_buff.str().c_str()) ;

    cout << "Ecriture terminée !" << endl ;

}