/* Module de reception video */ 

#ifndef VIDEO_OUT_H
#define VIDEO_OUT_H

#include <systemc.h>
#include "image.h"

using namespace std ;

/* Définition du module */


SC_MODULE(VIDEO_OUT) { 

     // Ports 
    sc_in<bool> clk ;
    sc_in<bool> reset_n ;

    sc_in<unsigned char> pixel_in ;

    sc_in<bool> href ;
    sc_in<bool> vref ;

    // Constructeur 
    VIDEO_OUT(sc_module_name n, const string & out_n = "output") : 
        sc_module(n) 
    {
        cout << "Instanciation du module de sortie vidéo " << name() << endl ;
        SC_HAS_PROCESS(VIDEO_OUT) ;
        SC_METHOD(gen_image);
        sensitive << clk.pos() ;
        dont_initialize();

        //Création de l'image (allocation mémoire)
        image.height = HEIGHT ;
        image.width = WIDTH ;
        image.pixel = mem ;
    }

    private :

    //Constantes de taille pour les images
    static const int HEIGHT  = 576;
    static const int WIDTH   = 720;
    static const int fHEIGHT = HEIGHT+49;
    static const int fWIDTH  = WIDTH +154;

    // Méthodes

    //Traite les signaux et  génère les images
    void gen_image() ;

    //Ecris image dans un fichier "outputXX.png"
    void write_image() ;

    int current_image_number ;
    unsigned char mem[HEIGHT * WIDTH] ;
    Image image ; 
    bool old_vref ;
    uint crrt_pix ;


} ;

#endif //VIDEO_OUT_H
