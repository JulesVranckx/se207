#include "zoom.h"

void ZOOM::fill_buff() {

    if (!reset_n) {
        cout << "Module: "<< name() <<"... reset" << endl ;
        old_vref = false ; 
        x_pix = 0 ;
        y_pix = 0 ;
        EOL = 0 ;
        buff_full = true ;
        return ; 
    }

    bool new_image = vref_in & !old_vref ; 

    buff_full = y_pix > HEIGHT /4 ;

    if (new_image) { 
        x_pix = y_pix = 0 ;
    }

    if (href_in) {
        if(is_in(x_pix,y_pix)) {
            buff[x_pix][y_pix] = pixel_in ;
        } 
        x_pix = x_pix + 1 ;
        if (x_pix >= WIDTH) {
            x_pix = 0 ;
            y_pix = (y_pix + 1)  % HEIGHT ;
        }
    }

    if (!href_in) {
        compt += 1 ;
    }

    if ((!old_href) & href_in) {
        EOL = compt ;
        compt = 0 ;
    }

    old_vref = vref_in ; 
    old_href = href_in ;
}

void ZOOM::send_pic() {

    href_out = false ;
    vref_out = false ;
    pixel_out = 0  ;

    for (;;) {

        while(!buff_full) wait() ;

        for (int x = 0; x < HEIGHT; x++) {
            for (int y = 0 ; y < WIDTH; y++) {
                
                vref_out = (x < 3) ;

                href_out = true ;
                pixel_out = buff[x/2][y/2] ; 
                wait();

            }

            //Fin de ligne
            href_out = false ;
            pixel_out = 0 ;
            for (int i = 0 ; i < EOL ; i++) {
                wait() ;
            }
        }
    }
}