//Module réalisant un zoom x2 centré.
// On obtiendra ainsi un image de taille de même dimension, en ayant des pixels 
// dupliqués 4 fois. 

#ifndef ZOOM_H
#define ZOOM_H

#include <systemc.h>

using namespace std ;

SC_MODULE(ZOOM) {

    //Ports 
    sc_in<bool> clk ; 
    sc_in<bool> reset_n ;

    sc_in<unsigned char> pixel_in ; 
    sc_in<bool> href_in ; 
    sc_in<bool> vref_in ; 

    sc_out<unsigned char> pixel_out ; 
    sc_out<bool> vref_out ;
    sc_out<bool> href_out ;

    // Constructeur

    SC_HAS_PROCESS(ZOOM) ;

    ZOOM(sc_module_name n) :
        sc_module(n)
    {
        cout << "Instanciation du module de zoom" << endl ;

        SC_METHOD(fill_buff) ;
        sensitive << clk.pos() ; 
        async_reset_signal_is(reset_n, false) ;
        dont_initialize() ;

        SC_CTHREAD(send_pic, clk.pos()) ;
        async_reset_signal_is(reset_n, false) ;
    }

    //Constantes de taille pour les images
    static const int HEIGHT  = 576;
    static const int WIDTH   = 720;
    static const int fHEIGHT = HEIGHT+49;
    static const int fWIDTH  = WIDTH +154;
    static const int MAX_BUFF = 3 * WIDTH ; 

    // SC_METHOD and SC_CTHREAD

    //Receive the pixels and store them in a buffer before using
    void fill_buff() ;

    // Get the pixels in the buffer, applies the zoom and send them
    void send_pic() ;

    //Module objects
    unsigned char buff[WIDTH][HEIGHT] ;
    bool old_vref ;
    int x_pix, y_pix ;
    sc_signal<bool> buff_full; 
    

    //Synchronisationd des lignes
    sc_signal<int> EOL ;
    int compt ;
    bool old_href ;



    bool is_in(int x, int y) {
        bool x_cond = (x>WIDTH/4) & (x < 3*WIDTH/4);
        bool y_cond = (y>HEIGHT/4) & (y<3*HEIGHT/4) ;
        return x_cond & y_cond ;
    }


};



#endif //ZOOM_H 