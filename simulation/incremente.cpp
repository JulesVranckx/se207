#include <systemc.h>

int sc_main (int argc, char * argv[])
{

    if (argc != 2){
        cerr << "Wrong usage : Please use ./incremente MAX_TIME" << endl ;
        return 1 ;
    }
    sc_trace_file *trace_f;

    trace_f = sc_create_vcd_trace_file ("incr");
    // On peut aussi préciser l'unité de temps dans le fichier vcd
    trace_f->set_time_unit(1,SC_NS);

    sc_uint<8> N = atoi(argv[1]) ;
    sc_uint<8> n = 0 ;

    cout << "Be careful , N is compute modulo 256" << endl ;
    cout << "You choose N=" << N << endl ;


    sc_trace(trace_f, n, "n");

    // La simulation
    sc_start(10, SC_NS) ;

    for (uint i = 1; i < N + 1; i++){
        n = i ;
        sc_start(10, SC_NS);
    }

    // Ferme le fichier de trace
    // ne peut êter fait qu'à la fin de la simulation
    sc_close_vcd_trace_file(trace_f);

    return 0;
}