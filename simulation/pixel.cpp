#include <systemc.h>

// Le type pixel
struct pixel_t {
    sc_uint<5> R;
    sc_uint<6> G;
    sc_uint<5> B;
    // Mon contructeur
    pixel_t( sc_uint<5> _R = 0,sc_uint<6> _G = 0, sc_uint<5> _B=0): R(_R), G(_G), B(_B) {}

    bool operator == (const pixel_t &other) const {
        return (R == other.R) && (G == other.G) && (B == other.B) ;
    }

    friend ostream& operator << ( ostream& o, const pixel_t& P ) {
        o << "(" << P.R << "," << P.G << "," << P.B << ")" ;
        return o;
    }

    //Surcharge de l'addition (pix + entier)
    pixel_t operator+(const unsigned int n) const {
      sc_uint<5> r = R + n ;
      sc_uint<6> g = G + n ;
      sc_uint<5> b = B + n ;

      return {r,g,b} ;
    }

    //Surcharge de l'addition (pix + pix) 
    pixel_t operator+(const pixel_t & other) const { 
        sc_uint<5> r = R + other.R ; 
        sc_uint<6> g = G + other.G ;
        sc_uint<5> b = B + other.B ; 

        return {r,g,b} ;
    }
};


// surcharge de l'addition (entier + pixel)
pixel_t operator+(const unsigned int n, const pixel_t p) {
  return p+1;
}

// surcharge de la fonction sc_trace pour le type utilisateur
void sc_trace( sc_trace_file* _f, const pixel_t& _foo, const std::string& _s ) {
    sc_trace( _f, _foo.R, _s + "_R" );
    sc_trace( _f, _foo.G, _s + "_G" );
    sc_trace( _f, _foo.B, _s + "_B") ;
}


// Le test
int sc_main (int argc, char * argv[])
{
    if (argc != 2 ){
        cerr << "Wrong usage : Please use ./pixel MAX_VALUE" << endl ;
        return 1 ;
    }
    
    // Initialisation de la trace
    sc_trace_file * trace ;
    trace = sc_create_vcd_trace_file("pixel_trace");
    trace->set_time_unit(1,SC_NS);

    //Pixel
    sc_signal<pixel_t> my_pixel ;

    //Max Value
    int n = atoi(argv[1]) ; 

    // Simulation
    sc_trace(trace, my_pixel, "Pixel") ;

    sc_start(5,SC_NS) ;

    for (int i = 0; i < n ; i++) {
        my_pixel = my_pixel.read() + 3;
        sc_start(5,SC_NS);
    }

    sc_close_vcd_trace_file(trace) ;
 

   return 0 ;
}
