#include <systemc.h>

#define DUMP(x) cout << #x << ":" << (x) << endl ;

int sc_main(int argc, char * argv[])
{
    sc_int<10> p ;

    p = 256 ;

    sc_lv<10> t ;

    t = p ;
    t = ++p ; 

    DUMP(p)
    DUMP(t)

    sc_uint<25> s ;
    s = -195 ; 

    sc_int<3> g ; 
    g = 7 ; 

    DUMP(s)
    DUMP(g)

    long ret = g & s ;

    DUMP(ret)

    ret = ret & 35 ;

    DUMP(ret)

    return 0 ;
}
