#include <systemc.h>

#define DUMP(x) cout << #x << ":" << (x) << endl ;

int sc_main(int argc, char * argv[])
{
    cout << "---------------------- Test des sc_logic -------------------" << endl ;
    sc_logic a('x');
    sc_logic b ; 

    b = 3 ;
    
    DUMP(a)
    DUMP(b) 

    a = 1 ; 
    DUMP(a)

    a = 'j' ;
    DUMP(a)

    a = 2 ; 
    DUMP(a)

    sc_logic d = sc_logic(1) ; 

    DUMP(d)

    // Test des opérateurs 

    sc_logic vrai, faux, x, z ;
    vrai = 1 ;
    faux = 0 ;
    x = 'x' ;
    z = 'z' ;

    DUMP(~vrai)
    DUMP(~x)
    DUMP(~z)
    DUMP(vrai & faux)
    DUMP(vrai | faux)
    DUMP(vrai & x)
    DUMP(vrai & z)
    DUMP(vrai | z)
    
    
    // Test avec des bool

    DUMP(vrai & true)
    DUMP(faux | true)
    DUMP(x | true)


    cout << "---------------------- Test des sc_lv -------------------" << endl ;

    sc_lv<7> tab = "1xx01z0" ;

    DUMP(tab)

    sc_lv<3> t ;
    t = 12 ;
    DUMP(t)
    t = 125 ; 
    DUMP(t) 

    tab(6,4) = tab(2,0) ;
    DUMP(tab)

    sc_lv<10> big ;
    
    big = (t,tab) ;


    DUMP(big)

    int n = tab.to_int() ;
    DUMP(n)

    DUMP(tab[5])

    tab = false ;
    DUMP(tab)

    bool val = tab.and_reduce() ; 
    DUMP(val)

    tab[4] = true ;
    DUMP(tab)
    DUMP(tab.or_reduce())


    return 0 ;
    
}
