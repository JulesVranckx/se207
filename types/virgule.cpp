#define SC_INCLUDE_FX
#include <systemc.h>
#include <cmath>

#define DUMP(x) cout << #x << ":" << (x) << endl ;

using namespace std ;

double compare(){
    
    sc_fix f ; 

    double max_error = 0 ;
    double local_error ; 

    //Compute max error
    for (int i =0; i < 128; i++){
        double d = M_PI * sin(M_PI * i / 256) ;
        f = d ;

        local_error = (abs(d-f)) ;

        if (local_error > max_error){
            max_error = local_error ;
        }
    }

    return max_error ;
}

int sc_main(int argc, char * argv[])
{

    if (argc != 2){

        cout << "Wrong usage!" << endl ;
        cout << "Plesae use ./virgule precision" << endl ;
        return 1 ;
    }
        
        
    cout << "################## Exercice sinus ################" << endl ;

    int partie_entiere = 4 ; 
    int partie_decimale = 1 ;

    try {
        double epsilon = stof(argv[1]) ; 
        double max_error ;

        bool again = true ;

        while(again) {

            sc_fxtype_params param(partie_decimale + partie_entiere, partie_entiere) ;
            sc_fxtype_context ctxt(param) ;
            
            max_error = compare() ;
            
            if (max_error > epsilon)
                //Increase size 
                partie_decimale++ ; 
            else 
                again = false ;  
    
            
        }

        cout << "Need " << partie_decimale << " bits with precision " << epsilon << endl ;
        
        return 0 ;
        }
    catch (const invalid_argument& ia){
        cerr << "Invalid Argument:" << ia.what() << endl ;
        return 1 ;
    }
    catch (const out_of_range& oor){
        cerr << "Argument out of range: " << oor.what() << endl ;
        return 2 ;
    }

    
} 
